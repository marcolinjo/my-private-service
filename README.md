# Wie unterschiedlich berechtigte Entwickler an gemeinsamen Code-Repositories arbeiten können?


Mit #Git-Submodules können Entwickler mit unterschiedlichen Berechtigungen an Teilen des Repositories bauen. Das ist zum Beispiel der Fall, wenn wir die Entwicklung der HTML, CSS, JS Bestandteil durch anonyme Fremde in öffentlichen Repositories vergeben wollen. 

## Der Ablauf

1. Angenommen Du hast bereits ein Projekt mit unterschiedlichen Verzeichnissen in einen Projektverzeichnis: <code>my-private-service/</code><br>
2. Zusätzlich hast Du ein Git-Repository gefunden, welches Du einbinden möchtest und dessen URL festgestellt z.B. [https://gitlab.com/marcolinjo/open-task.git](https://gitlab.com/marcolinjo/open-task.git)
2. Du wechselst in das Projek-Verzeichnis und führst folgenden Befehl in der Shell aus: <code>git submodule add https://gitlab.com/marcolinjo/open-task.git . </code><br> Der Befehl cloned dir das öffentliche Repo direkt in die angegebene Stelle.
3. Im Webinterface deines Repo findest Du jezt einen Zahlencode hinter dem Verzeichnis: [https://gitlab.com/marcolinjo/my-private-service](https://gitlab.com/marcolinjo/my-private-service)
4. Mit jedem Pull wird jetzt auch das Submodule aktualisiert. 


## Weitere Informationen 

[Eine gute Beschreibung für Submodules in Git](https://git-scm.com/book/de/v2/Git-Tools-Submodule)
<br>[Die beste Git-Beschreibung](https://rogerdudler.github.io/git-guide/index.de.html)